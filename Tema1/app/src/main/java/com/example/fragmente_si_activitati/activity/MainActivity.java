package com.example.fragmente_si_activitati.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;

import com.example.fragmente_si_activitati.R;
import com.example.fragmente_si_activitati.interfaces.ActivityFragmentCommunication;

public class MainActivity extends AppCompatActivity implements ActivityFragmentCommunication {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void openMainActivity2() {
        Intent intent=new Intent(this, MainActivity2.class);
        startActivity(intent);
    }

    @Override
    public void addFirstFragment() {

    }

    @Override
    public void addSecondFragment() {

    }

    @Override
    public void replaceWithFragment3() {

    }

    @Override
    public void closeActivity() {

    }

    @Override
    public void goBackToFragment1() {

    }


}